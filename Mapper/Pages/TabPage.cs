﻿using System;

using Xamarin.Forms;

namespace Mapper
{
	public class TabPage : TabbedPage
	{
		public TabPage ()
		{
			this.Children.Add(new FollowPage());
			this.Children.Add(new MapPage());
		}
	}
}


