﻿using System;

using Xamarin.Forms;

namespace Mapper
{
	public class MainPage : ContentPage
	{
		public MainPage ()
		{
			Label lblWelcome = new Label 
			{
				Text = "Welcome To Mapper",
				TextColor = Color.Aqua,
				XAlign = TextAlignment.Center
			};
			Entry eUserName = new Entry 
			{
				WidthRequest = 150,
				HeightRequest = 50,
				Placeholder = "Please Enter Your Name..."
			};
			Entry eUserPassword = new Entry 
			{
				WidthRequest = 150,
				HeightRequest = 50,
				Placeholder = "Please Enter Your Password..."
			};
			Button btnInit = new Button 
			{
				Text = "Login",
				WidthRequest = 150,
				HeightRequest = 50,
				BackgroundColor = Color.Aqua,
				TextColor = Color.Red
			};

			btnInit.Clicked += (sender, e) => {
				Navigation.PushModalAsync(new TabPage());
			};

			Content = new StackLayout
			{
				Children=
				{
					lblWelcome,
					eUserName,
					eUserPassword,
					btnInit
				},
				Padding = 20,
				Spacing = 10
			};


		}
	}
}


